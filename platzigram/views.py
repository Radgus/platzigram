from django.http import HttpResponse

from datetime import datetime
import json


def hello_world(request):
    return HttpResponse('Hello world')


def hi(request):
    numbers = [int(i) for i in request.GET['numbers'].split(',')]
    sorted_ints = sorted(numbers)
    data = {
        'status': 'ok',
        'numbers': sorted_ints,
        'message': 'Integer sorted successfully.',
    }
    return HttpResponse(json.dumps(data), content_type = 'application/json')


def parameters(request,name,age):
    message = 'like someone said {} year ago, {} what beautyful eyes you have'.format(age,name)
    return HttpResponse(message)
