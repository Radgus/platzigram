from django.shortcuts import render
# from django.http import HttpResponse
from datetime import datetime


# Create your views here.

posts = [
    {
        'title': 'clasic',
        'user': {
            'name': 'Vayne',
            'picture': 'http://placeimg.com/100/40/people/sepia',
        },
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo': 'http://placeimg.com/100/40/people/sepia',
    },
        {
        'title': 'luz',
        'user': {
            'name': 'Miss Fortune',
            'picture': 'http://placeimg.com/100/40/animals',
        },
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo': 'http://placeimg.com/100/40/animals',
    }, 
]


def list_posts(request):
    return render(request, 'feed.html',{'posts':posts})
